#!/usr/bin/env nextflow

// fastqc
process FASTQC {

	publishDir "${params.out_dir}/fastqc", mode: 'copy', overwrite: true

	input:
	tuple val(replicateId), path(reads) 

	output:
	path '*.html'

	script:
	"""
	conda run -n env fastqc -t 2 ${reads}
	"""
}

// read trimming
process TRIM {

	publishDir "${params.out_dir}/trimfastq", mode: 'symlink', overwrite: true

	input:
	tuple val(replicateId), path(reads) 

	output:
	tuple val(replicateId), path('*.fq.gz') 

	script:
 	def c_r1 = params.clip_r1 > 0 ? "--clip_r1 ${params.clip_r1}" : ''
   	def c_r2 = params.clip_r2 > 0 ? "--clip_r2 ${params.clip_r2}" : ''
    	def tpc_r1 = params.three_prime_clip_r1 > 0 ? "--three_prime_clip_r1 ${params.three_prime_clip_r1}" : ''
	def tpc_r2 = params.three_prime_clip_r2 > 0 ? "--three_prime_clip_r2 ${params.three_prime_clip_r2}" : ''

	"""
	conda run -n env trim_galore \
	-q 30 \
	--paired \
	--fastqc \
	--gzip \
	-j 4 \
	${c_r1} \
	${c_r2} \
	${tpc_r1} \
	${tpc_r2} \
	${reads}

	mkdir -p "${params.out_dir}/trimfastq"
	cp *.{html,txt} "${params.out_dir}/trimfastq"
	"""
}

// BWA_MEM2 Index
process BWA_MEM2_INDEX {

	if(params.keep_bwa_mem2_index){

		publishDir "${params.out_dir}", mode: 'copy', overwrite: true
	}
	
	input:
	path genome

	output:
	path 'BWA_MEM2_Index', type: 'dir'

	script:

	"""
	mkdir -p 'BWA_MEM2_Index'

	conda run -n env bwa-mem2 \
		index \
		-p ./BWA_MEM2_Index/ \
		${genome}
	"""
}

// chromosomes size
process CHR_SIZE {

    publishDir "${params.out_dir}/chr", mode: 'copy', overwrite: true

	input:
	path genome

	output:
	path "gsize.txt", emit:gsize
	path "chr_size.txt", emit: chr

	script:
	"""
    zcat ${genome} | sed 's/ .*//g' > genome.fa
	conda run -n env seqkit fx2tab -l -n genome.fa > chr_size.txt
	sed -i "s/ .*REF//g" chr_size.txt

	awk '{sum+=\$2;} END{print sum;}' chr_size.txt > gsize.txt
	"""
}

// BWA_MEM2 map
process BWA_MEM2_MAP {

	tag "${replicateId}"
	
	publishDir "${params.out_dir}/bam", mode: 'symlink', overwrite: true

	input:
	path genome
	tuple val(replicateId), path(reads)

	output:
	tuple val(replicateId), path('*.sorted.bam')

	script:
    score = params.bwa_mem2_min_score ? "-T ${params.bwa_mem2_min_score}" : ''
	"""
	conda run -n env bwa-mem2 mem \
		-t 10 \
        	-M \
        	-R '@RG\tID:${replicateId}' \
		${score} \
		${genome}/ \
		${reads} > tmp.sam

	conda run -n env samtools view -@ 10 -b -h -F 0x0100 -O BAM -o ${replicateId}.bam tmp.sam || true

    rm tmp.sam

	conda run -n env samtools sort \
		-@ 10 \
		-o ${replicateId}.sorted.bam \
		${replicateId}.bam
	"""
}

// MARK_DUPLICATE
process MARK_DUPLICATE {

	tag "${replicateId}"

	publishDir "${params.out_dir}/bam", mode: 'symlink', overwrite: true

	input:
	tuple val(replicateId), path(bam)

	output:
	tuple val(replicateId), path('*.sorted.mdup.bam')

	script:
	"""
	conda run -n picard picard -Xmx10G MarkDuplicates \
    	-I ${bam} \
    	-O ${replicateId}.sorted.mdup.bam \
    	-M mdup.metrics \
		-AS true \
		--VALIDATION_STRINGENCY LENIENT \
		--TMP_DIR tmp

	mkdir -p "${params.out_dir}/bam"

	conda run -n env samtools stats \
		${replicateId}.sorted.mdup.bam \
		> ${replicateId}.sorted.mdup.bam.stats

	cp *.stats "${params.out_dir}/bam"
	"""
}

// BAM_FILTER
process BAM_FILTER {

	tag "${replicateId}"

	input:
	tuple val(replicateId), path(bam)

	output:
	tuple val(replicateId), path('*.sorted.filtered.bam')

	script:
	"""
	conda run -n env samtools view \
	-@ 10 \
	-F 0x004 \
	-F 0x0008 \
	-f 0x001 \
	-F 0x0400 \
	-q 1 \
    -b \
	-o input.bam \
	${bam}

	conda run -n env bamtools filter \
		-in input.bam \
    	-out tmp.bam \
    	-script "$baseDir/script/bamtools_filter_pe_${params.method}.json"

	rm input.bam

	conda run -n env samtools sort \
		-@ 10 \
		-n \
		-o tmp.sorted_name.bam \
		tmp.bam
	
	rm tmp.bam

	conda run -n env python "$baseDir/bin/bampe_rm_orphan.py" \
		tmp.sorted_name.bam \
		${replicateId}.sorted_name.filtered.bam \
		--only_fr_pairs

	rm tmp.sorted_name.bam

	conda run -n env samtools sort \
		-@ 10 \
		-o ${replicateId}.sorted.filtered.bam \
		${replicateId}.sorted_name.filtered.bam
	
	rm ${replicateId}.sorted_name.filtered.bam

	conda run -n env samtools index ${replicateId}.sorted.filtered.bam

    conda run -n env samtools stats \
		${replicateId}.sorted.filtered.bam \
		> ${replicateId}.sorted.filtered.bam.stats

	mkdir -p "${params.out_dir}/bam"

	cp *.{sorted.filtered.bam,stats,bai} "${params.out_dir}/bam"
	"""
}

// BIGWIG
process BIGWIG {

	tag "${replicateId}"

	publishDir "${params.out_dir}/bigwig", mode: 'copy', overwrite: true

	input:
	path genome
	tuple val(replicateId), path(bam)

	output:
	tuple val(replicateId), path('*.bw')

	script:
	"""
	conda run -n env samtools flagstat ${bam} > flagstat

    SCALE_FACTOR=\$(grep 'mapped (' flagstat | awk 'NR==1{print \$1}')

    if [ \${SCALE_FACTOR} -gt 0 ]
	then
    
        SCALE_FACTOR=\$(echo \${SCALE_FACTOR} | awk 'NR==1{print 1000000/\$0}')

        conda run -n env genomeCoverageBed \
		    -ibam ${bam} \
		    -bg \
		    -scale \${SCALE_FACTOR} \
		    -pc \
	    | sort \
		    -T '.' \
		    -k1,1 \
		    -k2,2n > sample.bedGraph

        bedGraphToBigWig \
		    sample.bedGraph \
		    ${genome} \
		    ${replicateId}.bw
	else
		touch ${replicateId}_empty.bw
    fi
	"""
}
