#!/usr/bin/env nextflow

// parameters
params{

    // sequences
    input = false
    genome = false

    // bam input
    bam = false

    // method (ATAC or CHIP)
    method = false

    //fastqc
    skip_fastqc = false

    // trimming
    skip_trimming = false
    clip_r1 = 0
    clip_r2 = 0
    three_prime_clip_r1 = 0
    three_prime_clip_r2 = 0

    // bwa_mem2 options
    bwa_mem2_index = false
    keep_bwa_mem2_index = false
    bwa_mem2_min_score = false

    // skip markduplicates
    skip_markduplicates = false

    // skip filters
    skip_filters = false

    // skip bigwig
    skip_bigwig = false

    // save directory
    out_dir = "${PWD}/results"
}

// profiles
profiles {

    standard {
        process {
            // executor
            executor = 'local'
        }
    }

    slurm {

        process {
            // executor
            executor = 'slurm'

            // queue
            queue = 'workq'
        }
    }
}

// singularity container
singularity{
    enabled = true
//    autoMounts = true
    runOptions = '-B /work'
}

// process ressources
process{
    
    // Singularity container
    container = "$baseDir/singularity/genome_aln.sif"

    //fastqc
    withName: FASTQC {
        cpus = 2
	    memory = 5.GB
    }

    // trim
    withName: TRIM {
	    cpus = 5
	    memory = 5.GB
    }

    // BWA_MEM2 Index
    withName: BWA_MEM2_INDEX {
	    cpus = 1
	    memory = 60.GB
    }

    // chromosomes size
    withName: CHR_SIZE {  
	    cpus = 1
	    memory = 2.GB
    }

    // BWA_MEM2 map
    withName: BWA_MEM2_MAP {
	    cpus = 10
	    memory = 100.GB
    }

    // MARK_DUPLICATE
    withName: MARK_DUPLICATE {
	    cpus = 10
	    memory = 50.GB
    }

  // BAM_FILTER
    withName: BAM_FILTER {
	    cpus = 10
	    memory = 50.GB
    }

    // bigwig
    withName: BIGWIG {
	    cpus = 10
	    memory = 5.GB
    }
}

// Nextflow trace
trace {
    enabled = true
    overwrite= true
    file = 'trace.txt'
    fields = 'name,status,realtime,%cpu,%mem,rss,vmem,peak_rss,peak_vmem'
}

// Nextflow timeline
timeline {
  enabled = true
  file = "timeline.html"
}

// Nextflow DAG
dag {
    file = "dag.png"
}
