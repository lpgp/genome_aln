#!/usr/bin/env nextflow

// Enable DSL 2 syntax
nextflow.enable.dsl = 2

// Import modules 
include { 
	FASTQC;
	TRIM;
	BWA_MEM2_INDEX;
	CHR_SIZE;
	BWA_MEM2_MAP;
	MARK_DUPLICATE;
	BAM_FILTER;
	BIGWIG
} from "./modules/modules.nf"

// output directory
file("${params.out_dir}").mkdirs()

// main pipeline logic
workflow {

	if(params.input!=false & params.bam == false){

		// fastq input
		reads=Channel.fromPath(params.input)
			.splitCsv(header:true, sep:',')
    		.map {
        		row -> tuple(
            		row.ID,
            		[
						file(row.R1,checkIfExists: true),
						file(row.R2,checkIfExists: true),
					]
        		)
    		}

		//raw fastQC
		if(params.skip_fastqc==false){

			FASTQC(
				reads
			)
		}

		// trimming
		if(params.skip_trimming == false){

			TRIM (
				reads
			)

			tomap_ch=TRIM.out
		
		}else{

			tomap_ch = reads
		}

		// genome index channel
		if(params.bwa_mem2_index ==false){

			BWA_MEM2_INDEX (
				params.genome
			)

			bwa_mem2_index = BWA_MEM2_INDEX.out

		}else{

			bwa_mem2_index = params.bwa_mem2_index
		}

		// BWA-MEM2 Alignment
		BWA_MEM2_MAP(
			bwa_mem2_index,
			tomap_ch
		)

		bam = BWA_MEM2_MAP.out

	}else{

		// bam channel
		bam=Channel.fromPath(params.bam)
    		.map { file ->
        		def key = file.name.toString().tokenize('.').get(0)
        		return tuple(key, file)
     		}
	}

	// mark duplicates
	if(params.skip_markduplicates == false){

		MARK_DUPLICATE(
			bam
		)

		bam=MARK_DUPLICATE.out		
	}

	// filter alignements
	if(params.skip_filters == false){

		BAM_FILTER(
			bam
		)

	}

	// bigwig
	if(params.skip_bigwig == false){

		// compute chr sizes
		CHR_SIZE (
			params.genome
		)

		// bigwig
		BIGWIG(
			CHR_SIZE.out.chr,
			BAM_FILTER.out
		)
	}
}